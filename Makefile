.PHONY: build clean watch

clean:
		rm -rf dist

watch: build
		python3 -m http.server --directory dist 3003

build: clean
		mkdir dist
		rsync --exclude ".git/" \
			--exclude ".DS_Store" \
			--exclude ".*" \
			--exclude "Makefile" \
			--exclude "dist" \
			-avh ./ dist
